package pagesFlipkart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.SeMethods;

public class MobileDescPage extends SeMethods {
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Ratings')]")
	WebElement eleRatings;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reviews')]")
	WebElement eleReviews;

	public MobileDescPage() {
		PageFactory.initElements(driver, this);
	}

	public MobileDescPage VerifyTitlePage() {
		verifyTitle(text);

		return this;
	}

	public MobileDescPage GetRatingsReview() {
		System.out.println(eleReviews.getText());
		System.out.println(eleRatings.getText());

		return this;
	}

}
