package pagesFlipkart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.SeMethods;

public class MyHomePage extends SeMethods {
	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _29YdH8']")
	WebElement eleClose;
	@FindBy(how = How.XPATH, using = "//span[@class='_1QZ6fC _3Lgyp8']")
	WebElement eleClickElectronics;
	@FindBy(how = How.LINK_TEXT, using = "Mi")
	WebElement eleClickMi;

	public ListPage ClickElectronics() {
		click(eleClose);
		click(eleClickElectronics);
		click(eleClickMi);
		return new ListPage();
	}

}
