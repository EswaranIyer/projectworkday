package pagesFlipkart;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.SeMethods;

public class ListPage extends SeMethods {
	public ListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div[text()='Newest First']")
	WebElement eleNewFirst;
	@FindBy(how = How.XPATH, using = "//div[@class='_3wU53n']")
	WebElement eleFirstMobile;
	@FindBy(how = How.XPATH, using = "//div[@class='_3wU53n']")
	List<WebElement> eleListM;
	@FindBy(how = How.XPATH, using = "//div[@class='_1vC4OE _2rQ-NK']")
	List<WebElement> eleListPrice;

	public ListPage DisplayList() {
		for (WebElement eachBrand : eleListM) {
			System.out.println(eachBrand.getText());
		}

		return this;
	}

	public ListPage PriceDisplay() {
		for (WebElement eachPrice : eleListPrice) {
			System.out.println(eachPrice.getText());
		}
		return this;

	}

	public ListPage clickNewestFirst() {
		click(eleNewFirst);
		return this;
	}

	public MobileDescPage ClickFirstMobile() {
		text = eleFirstMobile.getText();
		
		click(eleFirstMobile);
		switchToWindow(1);
		return new MobileDescPage();
	}
}
