package beginner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Flipkart {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		driver.findElementByXPath("//span[@class='_1QZ6fC _3Lgyp8']").click();
		driver.findElementByLinkText("Mi").click();
		Thread.sleep(3000);
		String title = driver.getTitle();
		System.out.println("Title is : " + title);
		driver.findElementByXPath("//div[text()='Newest First']");
		List<WebElement> mobileList = driver.findElementsByXPath("//div[@class='_3wU53n']");
		for (WebElement eachBrand : mobileList) {
			System.out.println(eachBrand.getText());
		}

		List<WebElement> mobilePrice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for (WebElement eachPrice : mobilePrice) {
			System.out.println(eachPrice.getText().replaceAll("\\D", " "));
		}

		driver.findElementByXPath("//div[@class='_3wU53n']").click();
		Set<String> windowTitle = driver.getWindowHandles();
		ArrayList<String> list = new ArrayList<>();
		list.addAll(windowTitle);
		driver.switchTo().window(list.get(1));
		Thread.sleep(3000);
		String title2 = driver.getTitle();
		System.out.println("Title 2 : " + title2);
		driver.close();
		driver.switchTo().window(list.get(0));
		driver.close();
	}

}