package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pagesFlipkart.*;
import wdMethods.ProjectMethods;

public class TC001_FlipkartBrowse extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_FlipkartBrowse";
		testDescription = "LoginAndLogout";
		authors = "Eswaran";
		category = "Smoke";
		dataSheetName = "TC001";
		testNodes = "Leads";
	}

	@Test
	public void Browse() {
		new MyHomePage().ClickElectronics()
		.clickNewestFirst()
		.DisplayList()
		.PriceDisplay()
		.ClickFirstMobile()
		.VerifyTitlePage()
		.GetRatingsReview();

	}

}
